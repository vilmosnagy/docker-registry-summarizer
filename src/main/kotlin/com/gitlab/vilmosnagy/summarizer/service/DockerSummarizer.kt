package com.gitlab.vilmosnagy.summarizer.service

import com.gitlab.vilmosnagy.summarizer.pojo.DockerImageSize
import com.xenomachina.argparser.ArgParser
import java.nio.file.Path
import java.nio.file.Paths
import javax.inject.Inject

class DockerSummarizer
@Inject constructor(
        private val parser: ArgParser,
        private val registryReader: RegistryReader
) {

    private val path: Path by parser.storing(
            "-p", "--path",
            help = "Path to the root of the Docker Registry") { Paths.get(this@storing) }

    fun summarizeRegistry(): Collection<DockerImageSize> {
        val imageDirs = registryReader.resolveImageAndTagDirectories(path)
        return emptyList()
    }
}