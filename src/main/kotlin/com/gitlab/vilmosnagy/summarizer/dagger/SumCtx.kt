package com.gitlab.vilmosnagy.summarizer.dagger

import com.gitlab.vilmosnagy.summarizer.service.DockerSummarizer
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(SumModule::class))
interface SumCtx {

    companion object {
        fun getInstance(args: Array<String>) = DaggerSumCtx
                .builder()
                .sumModule(SumModule(args))
                .build()
    }

    @Singleton
    val summarizer: DockerSummarizer
}