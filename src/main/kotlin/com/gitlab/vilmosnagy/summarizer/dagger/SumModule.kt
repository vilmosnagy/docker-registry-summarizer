package com.gitlab.vilmosnagy.summarizer.dagger

import com.xenomachina.argparser.ArgParser
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

const val ARGUMENTS = "ARGUMENTS"

@Module
@Singleton
class SumModule(
        private val arguments: Array<String>
) {


    @Provides
    @Named(ARGUMENTS)
    fun provideArguments() = arguments

    @Provides
    @Singleton
    fun provideParser(@Named(ARGUMENTS) arguments: Array<String>) = ArgParser(arguments)
}