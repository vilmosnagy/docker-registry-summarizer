package com.gitlab.vilmosnagy.summarizer.pojo

data class DockerImage(
        val name: String,
        val tag: String
)