package com.gitlab.vilmosnagy.summarizer.pojo

import java.nio.file.Path

data class DockerLayer (
        val hash: String,
        val sizeInBytes: Long
)