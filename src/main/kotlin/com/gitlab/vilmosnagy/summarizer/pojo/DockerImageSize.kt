package com.gitlab.vilmosnagy.summarizer.pojo

data class DockerImageSize(
        val imageName: DockerImage,
        val layers: List<DockerLayer>
)