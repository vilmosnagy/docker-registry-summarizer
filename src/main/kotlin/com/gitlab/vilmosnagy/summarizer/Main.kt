package com.gitlab.vilmosnagy.summarizer

import com.gitlab.vilmosnagy.summarizer.dagger.SumCtx
import com.xenomachina.argparser.ArgParser

fun main(args: Array<String>) {
    val summed = SumCtx.getInstance(args).summarizer.summarizeRegistry()
    summed.forEach { println(it.toString())}
}